import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

df=pd.read_csv("/home/ritik/Downloads/primaryschool - primaryschool.csv")
item1=df['district_name'].tolist()
item2=df['cat'].tolist()
teams1=set(item2)
teams1=sorted(teams1)
city=set(item1)
dic={}
city=sorted(city)
for (i,j) in zip(item1,item2):
        if dic.get(i)==None:
                     temp={}
                     temp[j]=1
                     dic[i]=temp
        else:
                    temp=dic[i]
                    if temp.get(j)==None:
                        temp[j]=1
                    else:
                        temp[j]=1+temp.get(j)
                    
dics={}

for i in city :
       l=dic[i].keys()
       for v in teams1:
           if(dics.get(v)==None):
               if v in l:
                    list=[dic[i].get(v)]
                    dics[v]=list
               else:
                    list=[0]
                    dics[v]=list
           else:
               if v in l:
                  list=dics[v]
                  list.append(dic[i].get(v))
                  dics[v]=list
               else:
                  list=dics[v]
                  list.append(0)
                  dics[v]=list

# print(dics)
width=0.8
lenth=len(teams1)
print(teams1)
color=['red','yellow', 'black', 'blue', 'orange','cyan','gold','darkgreen','coral','chartreuse','lime','khaki','grey','olive']
bot=0

for i,c in zip(teams1,color):
        color=c
        print(color)
        plt.bar(city,dics[i] ,width,bottom=bot, color=c)
        bot=bot+np.array(dics[i])
plt.xticks(rotation=90)

print(dics)
print(dic)

plt.show()



