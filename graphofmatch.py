import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
df=pd.read_csv("/home/ritik/Downloads/matches - matches.csv")
# df =df.sort_values(['season'],axis=0,ascending=True,inplace=True,na_position='first')
item1=df['season'].tolist()
item2=df['team1'].tolist()
item3=df['team2'].tolist()
years=set(item1)
teams1=set(item2)
teams2=set(item3)
teams=teams1.union(teams2)
dic={}
years=sorted(years)
# teams=set(item1,item2)
for (i,j,k) in zip(item1,item2,item3):
        if dic.get(i)==None:
                     temp={}
                     temp[j]=1
                     temp[k]=1
                     dic[i]=temp
        else:
                    temp=dic[i]
                    if temp.get(j)==None:
                        temp[j]=1
                    else:
                        temp[j]=1+temp.get(j)
                    
                    if temp.get(k)==None:
                        temp[k]=1
                    else:
                        temp[k]=1+temp.get(k)
        


dics={}


for i in years :
       l=dic[i].keys()
       for v in teams:
           if(dics.get(v)==None):
               if v in l:
                    list=[dic[i].get(v)]
                    dics[v]=list
               else:
                    list=[0]
                    dics[v]=list
           else:
               if v in l:
                  list=dics[v]
                  list.append(dic[i].get(v))
                  dics[v]=list
               else:
                  list=dics[v]
                  list.append(0)
                  dics[v]=list

width=0.7
lenth=len(teams)
color=['red', 'yellow', 'black', 'blue', 'orange','cyan','gold','darkgreen','coral','chartreuse','lime','khaki','grey','olive']
bot=0

for i,c in zip(teams,color):
        print(c)
        plt.bar(years,dics[i] ,width,bottom=bot, color=c)
        bot=bot+np.array(dics[i])

plt.show()

